#WAP that provides functionality to convert decimal numbers to their binary representation and vice versa.

def decimal_to_binary(decimal_num: int) -> str:
    pass #replace this line with your own code

def binary_to_decimal(binary_str: str) -> int:
    pass #replace this line with your own code


# Test decimal to binary conversion
decimal_num = 10
print(f"Decimal: {decimal_num} -> Binary: {decimal_to_binary(decimal_num)}")

# Test binary to decimal conversion
binary_str = "1010"
print(f"Binary: {binary_str} -> Decimal: {binary_to_decimal(binary_str)}")
