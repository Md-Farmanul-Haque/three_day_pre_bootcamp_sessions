#WAP that calculates the factorial of numbers and 
# generates a list of factorials up to a specified limit.

def factorial(n: int) -> int:
    pass # replace this line with your own code

def generate_factorials(limit: int) -> list[int]:
    pass  # replace this line with your own code

limit = 10
factorials = generate_factorials(limit)
print(f"Factorials up to {limit}: {factorials}")
