def factorial(n: int) -> int:
    pass #replace this line with your own code

def is_strong_number(num: int) -> bool:
    pass #replace this line with your own code

def find_strong_numbers(limit: int) -> list[int]:
    pass #replace this line with your own code


# Check if a specific number is a strong number
number = 145
print(f"Is {number} a strong number? {is_strong_number(number)}")

# Find all strong numbers up to a limit
limit = 500
strong_numbers = find_strong_numbers(limit)
print(f"Strong numbers up to {limit}: {strong_numbers}")
