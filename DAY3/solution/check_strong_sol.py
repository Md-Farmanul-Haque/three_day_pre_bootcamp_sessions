def factorial(n: int) -> int:
    return 1 if n == 0 else n * factorial(n - 1)

def is_strong_number(num: int) -> bool:
    return num == sum(factorial(int(digit)) for digit in str(num))

def find_strong_numbers(limit: int) -> list[int]:
    return [num for num in range(1, limit + 1) if is_strong_number(num)]


# Check if a specific number is a strong number
number = 145
print(f"Is {number} a strong number? {is_strong_number(number)}")

# Find all strong numbers up to a limit
limit = 500
strong_numbers = find_strong_numbers(limit)
print(f"Strong numbers up to {limit}: {strong_numbers}")
