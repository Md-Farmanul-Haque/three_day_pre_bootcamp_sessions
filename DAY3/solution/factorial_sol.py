#WAP that calculates the factorial of numbers and 
# generates a list of factorials up to a specified limit.

def factorial(n: int) -> int:
    return 1 if n == 0 else n * factorial(n - 1)

def generate_factorials(limit: int) -> list[int]:
    return [factorial(i) for i in range(limit + 1)]


limit = 10
factorials = generate_factorials(limit)
print(f"Factorials up to {limit}: {factorials}")
