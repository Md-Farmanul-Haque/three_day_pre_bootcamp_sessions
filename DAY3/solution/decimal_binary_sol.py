#WAP that provides functionality to convert decimal numbers to their binary representation and vice versa.
def decimal_to_binary(decimal_num: int) -> str:
    if decimal_num == 0:
        return "0"
    binary_str = ""
    while decimal_num > 0:
        binary_str = str(decimal_num % 2) + binary_str
        decimal_num //= 2
    return binary_str

def binary_to_decimal(binary_str: str) -> int:
    return sum(int(bit) * (2 ** i) for i, bit in enumerate(reversed(binary_str)))


# Test decimal to binary conversion
decimal_num = 10
print(f"Decimal: {decimal_num} -> Binary: {decimal_to_binary(decimal_num)}")

# Test binary to decimal conversion
binary_str = "1010"
print(f"Binary: {binary_str} -> Decimal: {binary_to_decimal(binary_str)}")
