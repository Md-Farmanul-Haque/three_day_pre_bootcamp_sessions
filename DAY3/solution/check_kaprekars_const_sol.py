def num2digits(n: int) -> list[int]:
    return [int(ch) for ch in str(n)]
    
def digit2num(digits: list[int]) -> int:
    return int(''.join(str(digit) for digit in digits))

def largest(n: int) -> int:
    return digit2num(sorted(num2digits(n), reverse = True))

def smallest(n: int) -> int:
    return digit2num(sorted(num2digits(n)))


def next_kaprekkar(n: int) -> int:
    return largest(n) - smallest(n)

def keprekar_sequence(n: int) -> list[int]:
    seq = [n]
    n =  next_kaprekkar(n)
    while n != seq[-1]:
        seq.append(n)
        n = next_kaprekkar(n)
    return seq

print(keprekar_sequence(3542))
