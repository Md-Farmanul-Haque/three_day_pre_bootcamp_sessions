## Day 3: Problem Solving Session

Today's session focuses on solving four interesting problems to enhance your programming skills. Below is the plan and programs for the day:

### Problem 1: Factorial

- **Objective:** Implement a function to calculate the factorial of a given number.
- **Key Concepts:** Basic arithmetic operations, recursion.

### Problem 2: Strong Number

- **Objective:** Implement a function to check if a given number is a strong number.
- **Key Concepts:** Abstraction, reusability (using the factorial function from Problem 1), loops.

### Problem 3: Binary to Decimal

- **Objective:** Implement a function to convert a binary number to its decimal equivalent.
- **Key Concepts:** Enumerate function, binary representation.

### Problem 4: Kaprekar's Constant

- **Objective:** Implement a function to find Kaprekar's constant for a given number.
- **Key Concepts:** Sorting algorithms (using sorted()), list manipulation.
