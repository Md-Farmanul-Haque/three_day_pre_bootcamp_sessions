# Immutability in Python

## Introduction

Immutability refers to the concept where an object's state cannot be modified after it is created. In Python, immutable data types include `int`, `float`, `bool`, `str`, `tuple`, and `frozenset`. Understanding and leveraging immutability can lead to more predictable and error-resistant code.

## Immutable Data Types in Python

### Integers

```python
a = 10
b = a
b += 5

print(a)  # Output: 10
print(b)  # Output: 15
```

In this example, modifying `b` does not affect `a` because integers are immutable.

### Strings

```python
s = "hello"
t = s
t += " world"

print(s)  # Output: "hello"
print(t)  # Output: "hello world"
```

Strings are immutable, so concatenating to `t` does not change `s`.

### Tuples

```python
tuple1 = (1, 2, 3)
tuple2 = tuple1
tuple2 += (4, 5)

print(tuple1)  # Output: (1, 2, 3)
print(tuple2)  # Output: (1, 2, 3, 4, 5)
```

Modifying `tuple2` by adding elements does not affect `tuple1`.

## Advantages of Immutability

1. **Predictability**: Immutable objects do not change state, making the behavior of the program more predictable.

2. **Thread Safety**: Immutability ensures that objects are inherently thread-safe, as they cannot be modified by multiple threads simultaneously.

3. **Ease of Understanding**: Functions and methods that use immutable objects are easier to understand and reason about since their inputs do not change.

## Working with Immutability

### Creating New Objects

Instead of modifying an object, create a new object with the desired changes.

```python
original_tuple = (1, 2, 3)
new_tuple = original_tuple + (4, 5)

print(original_tuple)  # Output: (1, 2, 3)
print(new_tuple)       # Output: (1, 2, 3, 4, 5)
```

### Using Built-In Functions

Many built-in functions and methods return new objects rather than modifying the existing ones.

```python
s = "hello"
new_s = s.upper()

print(s)     # Output: "hello"
print(new_s) # Output: "HELLO"
```

## Custom Functions for Immutability

You can create custom functions to handle immutable operations on data types.

### Immutable String Concatenation

```python
def add_suffix_immutable(original: str, suffix: str) -> str:
    return original + suffix

original_string = "immutable"
new_string = add_suffix_immutable(original_string, " string")

print(original_string)  # Output: "immutable"
print(new_string)       # Output: "immutable string"
```

### Immutable List Update

Since lists are mutable, you can create a function that returns a new list with the updated value.

```python
def update_list_immutable(original_list: list, index: int, value: int) -> list[int]:
    new_list = original_list[:index] + [value] + original_list[index + 1:]
    return new_list

original_list = [1, 2, 3]
new_list = update_list_immutable(original_list, 1, 99)

print(original_list)  # Output: [1, 2, 3]
print(new_list)       # Output: [1, 99, 3]
```

### Immutable Dictionary Update

Similarly, for dictionaries, you can create a function that returns a new dictionary with the updated key-value pair.

```python
def update_dict_immutable(original_dict: dict, key: str, value : int) -> dict():
    new_dict = original_dict.copy()
    new_dict[key] = value
    return new_dict

original_dict = {"a": 1, "b": 2}
new_dict = update_dict_immutable(original_dict, "b", 99)

print(original_dict)  # Output: {"a": 1, "b": 2}
print(new_dict)       # Output: {"a": 1, "b": 99}
```

## Conclusion

Immutability is a powerful concept that can lead to safer and more maintainable code. By using immutable data types and designing custom functions that maintain immutability, you can take advantage of the benefits of immutability in your Python programs.