# First-Class Functions in Python

## Introduction

In Python, functions are treated as first-class citizens. This means that functions can be passed around and used as arguments, returned from other functions, and assigned to variables. This concept is fundamental to functional programming and allows for more flexible and reusable code.

## Functions as First-Class Citizens

### Assigning Functions to Variables

You can assign a function to a variable, allowing you to call the function using the variable name.

```python
def greet(name: str) -> str:
    return f"Hello, {name}!"

greeting = greet
print(greeting("Alice"))  # Output: Hello, Alice!
```

### Passing Functions as Arguments

Functions can be passed as arguments to other functions.

```python
def add(a: int, b: int) -> int:
    return a + b

def subtract(a: int, b: int) -> int:
    return a - b

def apply_operation(operation: callable, x: int, y: int) -> int:
    return operation(x, y)

print(apply_operation(add, 5, 3))        # Output: 8
print(apply_operation(subtract, 5, 3))   # Output: 2
```

### Returning Functions from Functions

Functions can return other functions.

```python
def create_multiplier(n: int) -> callable:
    def multiplier(x: int) -> int:
        return x * n
    return multiplier

double = create_multiplier(2)
triple = create_multiplier(3)

print(double(5))  # Output: 10
print(triple(5))  # Output: 15
```

### Storing Functions in Data Structures

Functions can be stored in data structures like lists and dictionaries.

```python
def square(x: int) -> int:
    return x * x

def cube(x: int) -> int:
    return x * x * x

functions = [square, cube]
for func in functions:
    print(func(3))  # Output: 9, then 27

operations = {
    "add": add,
    "subtract": subtract
}

print(operations["add"](10, 5))        # Output: 15
print(operations["subtract"](10, 5))   # Output: 5
```

## Higher-Order Functions

A higher-order function is a function that takes one or more functions as arguments or returns a function as a result.

### Map, Filter, and Reduce

Python provides several higher-order functions like `map()`, `filter()`, and `reduce()`.

```python
# Using map to apply a function to all items in a list
numbers = [1, 2, 3, 4]
squared = map(square, numbers)
print(list(squared))  # Output: [1, 4, 9, 16]

# Using filter to select items from a list
def is_even(n: int) -> bool:
    return n % 2 == 0

evens = filter(is_even, numbers)
print(list(evens))  # Output: [2, 4]

# Using reduce to aggregate items in a list
from functools import reduce

def multiply(a: int, b: int) -> int:
    return a * b

product = reduce(multiply, numbers)
print(product)  # Output: 24
```

## Decorators

Decorators are a powerful feature in Python that allow you to modify the behavior of a function. They are a type of higher-order function.

```python
def my_decorator(func: callable) -> callable:
    def wrapper() -> None:
        print("Something is happening before the function is called.")
        func()
        print("Something is happening after the function is called.")
    return wrapper

@my_decorator
def say_hello() -> None:
    print("Hello!")

say_hello()
```

Output:
```
Something is happening before the function is called.
Hello!
Something is happening after the function is called.
```

## Conclusion

Treating functions as first-class citizens allows for more flexible and powerful programming constructs. By understanding and utilizing this concept, you can write more modular, reusable, and expressive code in Python.