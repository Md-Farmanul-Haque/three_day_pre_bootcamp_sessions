### Windows Subsystem for Linux (WSL) Installation Guide

This guide will help set up Windows Subsystem for Linux (WSL) on a Windows machine, allowing a Linux distribution to run alongside the Windows OS.

---

### Step 1: Enable WSL

1. **Open PowerShell as Administrator:**
   - Press `Windows Key + X`, then select `Windows PowerShell (Admin)`.

2. **Run the following command to enable WSL:**
   ```powershell
   wsl --install
   ```
   This command will install the required components and the default Linux distribution (usually Ubuntu).

3. **Restart the computer** when prompted.


4. **Enable Virtual Machine Platform and WSL via Programs and Features:**

   - Press `Windows Key + S`, type `Windows Features`, and select `Turn Windows features on or off`.

   - In the Windows Features dialog, check the boxes for:

     - `Virtual Machine Platform`

     - `Windows Subsystem for Linux`

   - Click `OK` and restart the computer if prompted.

---

### Step 2: Install a Linux Distribution

If a different Linux distribution other than the default Ubuntu is preferred, follow these steps:

1. **Open the Microsoft Store** and search for "Linux".

2. **Select the preferred Linux distribution** (e.g., Ubuntu, Debian, Kali Linux) and click `Get` to install it.

---

### Step 3: Set Up the Linux Distribution

1. **Launch the installed Linux distribution** from the Start menu.

2. **Complete the initial setup** by creating a new user account and setting up a password.

---

### Step 4: Update and Upgrade Packages

1. **Open the Linux distribution**.

2. **Update the package list** and upgrade installed packages by running the following commands:

   ```bash
   sudo apt update
   sudo apt upgrade -y
   ```

---

### Step 5: Verify Installation

To ensure WSL is correctly installed and running, open the Linux distribution and run a simple command, such as:

```bash
uname -a
```

This command should return system information indicating that a Linux kernel is running within WSL.

---

### Additional Tips


- **Accessing Windows Files:**
  Windows files can be accessed from within WSL at `/mnt/c/`. For example, the C: drive is mounted at `/mnt/c/`.


- **Switching Between WSL Versions:**
  If there is a need to switch between WSL 1 and WSL 2, use the following commands:

  ```powershell
  # Set WSL 2 as the default version
  wsl --set-default-version 2

  # Set a specific distribution to use WSL 2
  wsl --set-version <distribution_name> 2
  ```

- **Check WSL Version:**
  To check which version of WSL the distributions are using, run:

  ```powershell
  wsl -l -v
  ```

---

### Resources

- **Official WSL Documentation:**
  For more detailed information and troubleshooting, visit the [official WSL documentation](https://docs.microsoft.com/en-us/windows/wsl/).

---

By following these steps, WSL should be set up and running on the Windows machine, providing a full Linux environment for development and other tasks.