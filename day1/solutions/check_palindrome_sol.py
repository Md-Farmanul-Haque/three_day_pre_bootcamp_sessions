check_string  = "#@!!@#"

def reverse_of_string(check_string: str) -> str:
    reversed_string = ""
    for i in range(len(check_string) - 1,-1,-1):
        reversed_string += check_string[i]
    return reversed_string


reverse_string_result = reverse_of_string(check_string)
print(f'Reversed string of {check_string} is {reverse_string_result}')


''' Second approach for reverse '''

def reverse_string2(check_string: str) -> str:
    return check_string[::-1]




def check_palindrome(check_string: str) -> bool:
    return check_string == reverse_string2(check_string)







check_result = check_palindrome(check_string)

print(f"{check_string} is a Palindrome ? {check_result}")
print(reverse_string2("Hello"))
