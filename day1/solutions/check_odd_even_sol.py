'''
Problem Statement: WAP to check whether a give number is odd or even
'''
def check_odd_even(n : int) -> bool:
    if (n % 2 == 0 ):
        return True
    else:
        return False






''' Second Approach '''

def check_odd_even2(n : int) -> bool:
    return n % 2 == 0

n = 12

check_result  = check_odd_even(n)
check_result2 = check_odd_even2(n)

print(f"{n} is Even : {check_result}")
print(f"{n} is Even : {check_result2}")
