'''
Problem Statement : WAP to count the vowels in a given sample string.
'''
vowels = 'AEIOUaeiou'
def count_vowel(sample_string: str) -> int:
    count = 0
    for word in sample_string:
        if word in vowels:
            count += 1
    return count

    






sample_string  = "WE PROGRAM"

vowel_count = count_vowel(sample_string)

print(f'{sample_string} has  {vowel_count} vowels')

