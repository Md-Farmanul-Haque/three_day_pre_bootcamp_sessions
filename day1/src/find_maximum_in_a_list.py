'''
Problems Statement: Find Largest Element in a List.
'''

def find_largest_element(arr: list) -> int :

    pass  # Replace this line with your code

int_list = [12,13,67,45,55,1,0]

largest_element = find_max_element(int_list)

print(f"The largest element in {int_list} is {largest_element}")