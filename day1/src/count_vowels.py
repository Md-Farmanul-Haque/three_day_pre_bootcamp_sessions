'''
Problem Statement : WAP to count the vowels in a given sample string.
'''
vowels = 'AEIOUaeiou'
def count_vowel(sample_string: str) -> int:
    pass #replace this line with your codes


sample_string  = "WE PROGRAM"

vowel_count = count_vowel(sample_string)

print(f'{sample_string} has  {vowel_count} vowels')