'''
Problem Statement: Create a fibonacci series for a given number of terms.
'''

def generate_fibonacci(n : int) -> list[int] :
    pass  #replace this line with your own codes


n = 5

fibonacci_series = generate_fibonacci(n)

print(f"Fibonacci series upto {n} terms : {fibonacci_series}")