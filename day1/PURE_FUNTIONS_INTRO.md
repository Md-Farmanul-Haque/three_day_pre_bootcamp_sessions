# Pure Functions

## Introduction

Pure functions are a fundamental concept in functional programming. A pure function is a function that always produces the same output for the same input and has no side effects. This concept is crucial for writing predictable, testable, and maintainable code.


## Characteristics of Pure Functions

1. **Deterministic**: A pure function always returns the same result when given the same arguments. This means that the function's output depends solely on its input parameters.


2. **No Side Effects**: Pure functions do not cause any observable side effects outside the function. They do not modify any external state, such as changing a global variable, modifying an argument passed by reference, or performing I/O operations.

## Benefits of Pure Functions

- **Predictability**: Pure functions are predictable, making them easier to understand, test, and debug.

- **Testability**: Since pure functions depend only on their inputs, they can be tested in isolation without the need for a specific environment or state.

- **Composability**: Pure functions can be easily composed and reused to build more complex functions, leading to more modular and maintainable code.

## Examples

### Example of a Pure Function

```python
def add(a: int, b: int) -> int:
    return a + b

# Calling the function with the same inputs will always return the same result
print(add(2, 3))  # Output: 5
print(add(2, 3))  # Output: 5
```

### Example of an Impure Function

```python
result = 0

def add_to_result(value: int) -> int:
    
    global result

    result += value
    
    return result

# Calling the function with the same inputs can produce different results
print(add_to_result(5))  # Output: 5

print(add_to_result(5))  # Output: 10 (result has changed due to side effect)
```

## Conclusion

Pure functions are a cornerstone of functional programming, offering numerous benefits such as predictability, testability, and composability. By adhering to the principles of pure functions, developers can write cleaner, more maintainable, and reliable code.