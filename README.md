# Python Boot Camp Prep Sessions

Welcome to the prep sessions for our Python Boot Camp! These sessions are designed to get you acquainted with functional programming, good programming practices, number systems, and comprehensions before the main boot camp.

## Schedule
```
The prep sessions will take place over three days, with each session lasting two hours.
```

### Day 1: Introduction to Functional Programming

#### Topics Covered
```- Introduction to Functional Programming

- Key concepts: Pure functions, immutability, first-class functions

- Lambda functions in Python

- Map, Filter, and Reduce functions
```

#### Additional Installation (Day 1):

- Installing Windows Subsystem for Linux (WSL) for Windows users [WSL Installation Guide](https://docs.microsoft.com/en-us/windows/wsl/install)

### Day 2: Good Programming Practices

#### Topics Covered
```
- Code readability and maintainability

- Writing clean and modular code

- Documenting your code

- Error handling
````

### Day 3: Number Systems and Comprehensions

#### Topics Covered
```
- Understanding number systems: Binary, Decimal, Hexadecimal

- Practical applications of number systems in programming

- List comprehensions in Python

- Dictionary and set comprehensions

```
## Preparation Checklist

1. **Installations:**
   - Python (latest version) [Download Python](https://www.python.org/downloads/)
   
   - A code editor (e.g., VSCode, PyCharm) [VSCode](https://code.visualstudio.com/Download).
   
   - Windows Subsystem for Linux (WSL) [WSL Installation Guide](https://docs.microsoft.com/en-us/windows/wsl/install)


2. **Resources to Review:**

   - [Python Official Documentation](https://docs.python.org/3/)

   - [Functional Programming in Python](https://realpython.com/python-functional-programming/)

   - [Comprehensions in Python](https://realpython.com/list-comprehension-python/)

## Directory Structure

```plaintext
.
├── Day1
│   ├── README.md
│   └── functional_programming.py
├── Day2
│   ├── README.md
│   └── good_programming_practices.py
├── Day3
│   ├── README.md
│   └── number_systems_and_comprehensions.py
├── LICENSE
└── README.md
```

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Contact

For any queries, please reach out to [Farman](mailto:farmanul.h@talentsprint.com).
