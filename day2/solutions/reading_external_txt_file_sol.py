def read_file(file_path: str) -> str:
    with open(file_path, 'r') as file:
        return file.read()

#for user input
'''def get_target_character() -> str:
    return input("Enter the character to find the frequency of: ")'''

def count_character_frequency(input_string: str, target_char: str) -> int:
    return input_string.count(target_char)

def print_frequency(character: str, frequency: int) -> None:
    print(f"Character: {character}, Frequency: {frequency}")


file_path = 'file_path' #input("Enter the path to the text file: ")
input_string = read_file(file_path)
target_char = 't' #get_target_character()
frequency = count_character_frequency(input_string, target_char)
print_frequency(target_char, frequency)
