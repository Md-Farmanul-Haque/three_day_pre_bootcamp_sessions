
def get_input_string():
    return input("Enter a string: ")

def count_characters(input_string):
    return {char: input_string.count(char) for char in set(input_string)}

def print_frequency(frequency_dict):
    for char, freq in frequency_dict.items():
        print(f"Character: {char}, Frequency: {freq}")

input_string = "this is a string"
frequency_dict = count_characters(input_string)
print_frequency(frequency_dict)

