VOWELS = 'aeiouAEIOU'
CONSONANTS = ''.join([chr(i) for i in range(97, 123) if chr(i) not in VOWELS.lower()]) + ''.join([chr(i) for i in range(65, 91) if chr(i) not in VOWELS.upper()])

#this cam be used for taking user input
'''def get_input_string():
    return input("Enter a string: ")'''

def count_vowels_and_consonants(input_string):
    vowel_count = {char: input_string.count(char) for char in set(input_string) if char in VOWELS}
    consonant_count = {char: input_string.count(char) for char in set(input_string) if char in CONSONANTS}
    
    return vowel_count, consonant_count

def print_frequency(frequency_dict, label):
    print(f"\nFrequency of {label}:")
    for char, freq in frequency_dict.items():
        print(f"Character: {char}, Frequency: {freq}")


input_string = "this is a string " #get_input_string() -> use this incase of user input
# Filter out non-alphabetic characters
input_string = ''.join(filter(str.isalpha, input_string))
vowel_count, consonant_count = count_vowels_and_consonants(input_string)
print_frequency(vowel_count, "vowels")
print_frequency(consonant_count, "consonants")
