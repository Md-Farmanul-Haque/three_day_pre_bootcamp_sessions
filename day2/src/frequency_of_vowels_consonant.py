VOWELS = 'aeiouAEIOU'
CONSONANTS = ''.join([chr(i) for i in range(97, 123) if chr(i) not in VOWELS.lower()]) + ''.join([chr(i) for i in range(65, 91) if chr(i) not in VOWELS.upper()])

#incase of user input
'''def get_input_string():
    pass #replace this line with your own code'''

def count_vowels_and_consonants(input_string):
    pass # replace this line with your own code

def print_frequency(frequency_dict, label):
    pass #replace this line with your own code


input_string = "this is a string" # get_input_string() for the user input function
# Filter out non-alphabetic characters
input_string = ''.join(filter(str.isalpha, input_string))
vowel_count, consonant_count = count_vowels_and_consonants(input_string)
print_frequency(vowel_count, "vowels")
print_frequency(consonant_count, "consonants")
