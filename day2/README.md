## Day 2: Good Programming Practices

## Introduction to Day 2

Day 2 focuses on the essential principles of good programming practices. Adopting these practices helps in writing code that is functional, easy to understand, maintain, and extend. The key concepts covered today include:

1. **Code Readability and Maintainability:** Understanding why readable code is important and learning practical tips to improve the readability and maintainability of code.

2. **Writing Clean and Modular Code:** Exploring techniques to write clean, organized, and modular code that is easy to manage and scale.

3. **Documenting Code:** Learning how to effectively document code to facilitate better understanding and collaboration.

4. **Error Handling:** Understanding the importance of error handling and learning various techniques to manage errors gracefully in code.

---

### Topics Covered

#### 1. Code Readability and Maintainability

- **Importance of Readability:** Readable code is easier to understand, debug, and maintain. It facilitates collaboration and reduces the likelihood of errors.
  
- **Tips for Improving Readability:**
  
  - **Use Meaningful Variable and Function Names:** Avoid single-letter names and use descriptive names that convey the purpose.
  
  - **Consistent Naming Conventions:** Follow consistent naming conventions (e.g., snake_case for variables and functions, CamelCase for classes).
  
  - **Code Formatting:** Use indentation and whitespace effectively, and follow the PEP 8 style guide for Python.
  
  - **Comments and Documentation:** Write comments to explain the purpose of complex code blocks and avoid obvious comments.

#### 2. Writing Clean and Modular Code

- **Clean Code Principles:** Clean code is simple, direct, and easy to read. Key principles include simplicity, readability, and consistency.
  
- **Modularity:** Modular code is organized into distinct, independent modules that encapsulate specific functionality. Benefits of modularity include easier understanding, enhanced reusability, and facilitated testing.

#### 3. Documenting Code

- **Importance of Documentation:** Documentation helps developers understand the codebase, use functions correctly, and contribute effectively.
  
- **Types of Documentation:**
  
  - **Docstrings:** Used to describe modules, classes, and functions.
  
  - **Inline Comments:** Explain specific lines or blocks of code.
  - **README Files:** Provide an overview of the project, setup instructions, and usage examples.

#### 4. Error Handling

- **Importance of Error Handling:** Proper error handling ensures that a program can handle unexpected situations gracefully and continue operating or terminate cleanly.
  
- **Techniques for Error Handling:**
  
  - **Try-Except Blocks:** Catch and handle exceptions.
  
  - **Custom Exceptions:** Define and raise custom exceptions for specific error conditions.
  
  - **Finally Block:** Execute code regardless of whether an exception was raised.

---

### Example of Code Readability and Maintainability

```python
# Bad Example
def calc(x, y):
    return x * y + (x / y)

# Good Example
def calculate_area_of_rectangle(length, width):
    return length * width

def calculate_ratio(numerator, denominator):
    if denominator != 0:
        return numerator / denominator
    else:
        raise ValueError("Denominator cannot be zero")
```

By the end of the day, there should be a strong understanding of these good programming practices, and the ability to apply them to code to make it more readable, maintainable, and robust.